use std::borrow::Cow;

use serde::{Deserialize, Serialize};
use typed_builder::TypedBuilder;

#[derive(Deserialize, Debug)]
pub struct UserGameEntry {
    pub custom_title: String,
    pub game_id: u64,
}

#[derive(Deserialize)]
pub(super) struct UserGamesList {
    pub(super) data: Data,
}

#[derive(Deserialize)]
pub(super) struct Data {
    #[serde(rename = "gamesList")]
    pub(super) games_list: Vec<UserGameEntry>,
}

#[derive(Serialize, TypedBuilder)]
#[builder(field_defaults(default))]
pub struct UserGamesRequest<'a> {
    #[builder(!default)]
    pub(super) user_id: u64,
    #[builder(!default)]
    lists: Vec<Cow<'a, str>>,
    #[builder(default = "comp_all".into())]
    set_playstyle: Cow<'a, str>,
    #[serde(rename = "name")]
    game_title: Cow<'a, str>,
    platform: Cow<'a, str>,
    storefront: Cow<'a, str>,
    #[serde(rename = "sortBy")]
    sort_by: Cow<'a, str>,
    #[serde(rename = "sortFlip")]
    sort_flip: bool,
    view: Cow<'a, str>,
    random: bool,
    #[builder(default = 1000)]
    limit: u64,
    #[serde(rename = "currentUserHome")]
    current_user_home: bool,
}
