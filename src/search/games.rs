use serde::Serialize;
use std::borrow::Cow;

#[derive(Serialize)]
pub(super) struct Games<'a> {
    #[serde(rename = "userId")]
    user_id: u64,
    platform: String,
    #[serde(rename = "sortCategory")]
    sort_category: Cow<'a, str>,
    #[serde(rename = "rangeCategory")]
    range_category: String,
    #[serde(rename = "rangeTime")]
    range_time: RangeTime,
    gameplay: Gameplay<'a>,
    #[serde(rename = "rangeYear")]
    range_year: RangeYear<'a>,
    modifier: String,
}

impl Default for Games<'_> {
    fn default() -> Self {
        Self {
            user_id: Default::default(),
            platform: Default::default(),
            sort_category: "popular".into(),
            range_category: "main".into(),
            range_time: Default::default(),
            gameplay: Default::default(),
            range_year: Default::default(),
            modifier: Default::default(),
        }
    }
}

#[derive(Serialize, Default)]
struct RangeTime {
    min: Option<u64>,
    max: Option<u64>,
}

#[derive(Serialize, Default)]
struct Gameplay<'a> {
    perspective: Cow<'a, str>,
    flow: Cow<'a, str>,
    genre: Cow<'a, str>,
}

#[derive(Serialize, Default)]
struct RangeYear<'a> {
    min: Cow<'a, str>,
    max: Cow<'a, str>,
}
