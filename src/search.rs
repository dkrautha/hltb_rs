mod games;

use games::Games;
use serde::{Deserialize, Serialize};
use std::borrow::Cow;
use typed_builder::TypedBuilder;

#[derive(Serialize)]
pub enum SearchType {
    #[serde(rename = "games")]
    Games,
    #[serde(rename = "users")]
    Users,
}

#[derive(Serialize, TypedBuilder)]
pub struct SearchRequest<'a> {
    #[serde(rename = "searchType")]
    search_type: SearchType,

    #[serde(rename = "searchTerms")]
    search_terms: Vec<Cow<'a, str>>,

    #[serde(rename = "searchPage")]
    #[builder(default = 1)]
    search_page: u64,

    #[builder(default = 20)]
    size: u64,

    #[serde(rename = "searchOptions")]
    #[builder(default)]
    search_options: SearchOptions<'a>,
}

#[derive(Serialize, Default)]
pub struct SearchOptions<'a> {
    games: Games<'a>,
    users: Users<'a>,
    lists: Lists<'a>,
    filter: Cow<'a, str>,
    sort: bool,
    randomizer: bool,
}

#[derive(Serialize, Clone, Debug)]
struct Users<'a> {
    #[serde(rename = "sortCategory")]
    sort_category: Cow<'a, str>,
}

impl Default for Users<'_> {
    fn default() -> Self {
        Self {
            sort_category: "postcount".into(),
        }
    }
}

#[derive(Serialize)]
struct Lists<'a> {
    #[serde(rename = "sortCategory")]
    sort_category: Cow<'a, str>,
}

impl Default for Lists<'_> {
    fn default() -> Self {
        Self {
            sort_category: "follows".into(),
        }
    }
}

#[derive(Deserialize, Debug)]
pub(super) struct SearchResponse<'a> {
    pub(super) data: Vec<SearchGameEntry<'a>>,
}

#[derive(Deserialize, Debug)]
pub struct SearchGameEntry<'a> {
    pub game_id: u64,
    pub game_name: Cow<'a, str>,
    pub game_alias: Cow<'a, str>,
    pub comp_main: u64,
    pub comp_plus: u64,
    pub comp_100: u64,
    pub comp_all: u64,
}
