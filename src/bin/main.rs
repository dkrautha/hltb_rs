use std::borrow::Cow;

use anyhow::Result;
use hltb_rs::{HltbClient, SearchRequest, SearchType, UserGamesRequest};
use rand::{seq::SliceRandom, SeedableRng};

const LOWER_BOUND: u64 = 10;
const UPPER_BOUND: u64 = 26;

fn convert_seconds_to_hours_and_minutes(seconds: u64) -> (u64, u64) {
    let hours = seconds / 3600;
    let minutes = (seconds % 3600) / 60;
    (hours, minutes)
}

#[tokio::main]
async fn main() -> Result<()> {
    let client = HltbClient::new();
    let user_games_request = UserGamesRequest::builder()
        .user_id(466134)
        .lists(vec!["backlog".into()])
        .build();

    let user_games_entries = client.user_games_list(&user_games_request).await?;
    let mut rng = rand_chacha::ChaCha8Rng::seed_from_u64(42069);

    let mut fallback_counter = 0;

    loop {
        let choice = user_games_entries
            .choose(&mut rng)
            .expect("There should be at least one entry.");

        let search_request = SearchRequest::builder()
            .search_type(SearchType::Games)
            .search_terms(
                choice
                    .custom_title
                    .split_whitespace()
                    .map(Cow::from)
                    .collect(),
            )
            .size(1)
            .build();

        let search_response = client.search(&search_request).await?;

        if let Some(entry) = search_response.first() {
            let (hours, minutes) = convert_seconds_to_hours_and_minutes(entry.comp_100);

            let in_time_window = hours >= LOWER_BOUND && hours <= UPPER_BOUND;

            if in_time_window {
                println!(
                    "Found game: {}\nhours: {}, minutes: {}",
                    entry.game_name, hours, minutes
                );
                break;
            }
        }

        fallback_counter += 1;

        if fallback_counter > 100 {
            println!(
                "Tried to find a game between {} and {} hours within {} tries, giving up.",
                LOWER_BOUND, UPPER_BOUND, fallback_counter
            );
        }
    }

    Ok(())
}
