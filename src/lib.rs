mod search;
mod user;

pub use search::{SearchGameEntry, SearchRequest, SearchType};
pub use user::{UserGameEntry, UserGamesRequest};

use reqwest::{header, Client, IntoUrl, RequestBuilder};
use search::SearchResponse;
use user::UserGamesList;

const BASE_URL: &str = "https://howlongtobeat.com/api";
const USER_AGENT: &str = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) \
    AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36";

#[derive(Default)]
pub struct HltbClient {
    client: Client,
}

impl HltbClient {
    pub fn new() -> HltbClient {
        HltbClient {
            client: Client::new(),
        }
    }

    fn post(&self, endpoint: impl IntoUrl) -> RequestBuilder {
        self.client
            .post(endpoint)
            .header(header::USER_AGENT, USER_AGENT)
            .header("Content-type", "application/json; charset=UTF-8")
            .header("Accept", "*/*")
            .header("origin", "https://howlongtobeat.com")
            .header("referer", "https://howlongtobeat.com")
    }

    pub async fn user_games_list(
        &self,
        request: &UserGamesRequest<'_>,
    ) -> Result<Vec<UserGameEntry>, reqwest::Error> {
        let endpoint = format!("{}/user/{}/games/list", BASE_URL, request.user_id);
        let response = self.post(endpoint).json(request).send().await?;
        let response_text = response.text().await?;
        let decoded: UserGamesList = serde_json::from_str(&response_text).unwrap();
        Ok(decoded.data.games_list)
    }

    pub async fn search(
        &self,
        request: &SearchRequest<'_>,
    ) -> Result<Vec<SearchGameEntry>, reqwest::Error> {
        let endpoint = format!("{}/search", BASE_URL);
        let response = self.post(endpoint).json(&request).send().await?;
        let decoded: SearchResponse = response.json().await?;

        Ok(decoded.data)
    }
}
